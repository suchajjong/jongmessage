package com.jongzazaal.jongmessage.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

//
//  CallServiceModel.kt
//
//  JongMessage
//
//  Created by Jong on 1/4/2023 AD.
//
@Parcelize
data class CallServiceModel(
    var callId: String? = null,
    var callerId: String? = null,
    var callerNickName: String? = null,
    var isConnect: Boolean? = null,
    var amICallee: Boolean? = null,
    var isAccept: Boolean? = null


): Parcelable