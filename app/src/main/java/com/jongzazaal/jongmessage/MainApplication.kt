package com.jongzazaal.jongmessage

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.multidex.MultiDex
import com.jongzazaal.jongmessage.local.ContexterManager
import com.jongzazaal.jongmessage.service.CallService
import com.sendbird.android.SendBird
import com.sendbird.android.SendBirdException
import com.sendbird.android.handlers.InitResultHandler
import com.sendbird.calls.DirectCall
import com.sendbird.calls.SendBirdCall
import com.sendbird.calls.SendBirdCall.Options.addDirectCallSound
import com.sendbird.calls.SendBirdCall.SoundType
import com.sendbird.calls.handler.DirectCallListener
import com.sendbird.calls.handler.SendBirdCallListener
import java.util.*

//
//  MainApplication.kt
//
//  JongMessage
//
//  Created by Jong on 1/4/2023 AD.
//
class MainApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        ContexterManager.getInstance().setApplicationContext(this)
        ContexterManager.getInstance().setApplication(this)
        SendBird.init("43E88506-FB85-4971-8994-57A45F14C967", getApplicationContext(), false, object :
            InitResultHandler {
            override fun onInitFailed(e: SendBirdException) {
                Log.d("tag2", "onInitFailed")
            }

            override fun onInitSucceed() {
                Log.d("tag2", "onInitSucceed")
            }

            override fun onMigrationStarted() {
                Log.d("tag2", "onMigrationStarted")
            }

        })
        SendBirdCall.init(getApplicationContext(), "43E88506-FB85-4971-8994-57A45F14C967");
        SendBirdCall.addListener(UUID.randomUUID().toString(), object : SendBirdCallListener(){
            override fun onRinging(call: DirectCall) {
                CallService.onRinging(getApplicationContext(), call)
                call.setListener(object : DirectCallListener(){
                    override fun onConnected(call: DirectCall) {

                    }

                    override fun onEnded(call: DirectCall) {
                        val ongoingCallCount = SendBirdCall.ongoingCallCount
                        if (ongoingCallCount == 0) {
                            CallService.stopService(getApplicationContext())
                        }
                    }

                })
            }

        })
        addDirectCallSound(SoundType.DIALING, R.raw.dialing)
        addDirectCallSound(SoundType.RINGING, R.raw.ringing)
        addDirectCallSound(SoundType.RECONNECTING, R.raw.reconnecting)
        addDirectCallSound(SoundType.RECONNECTED, R.raw.reconnected)
    }
    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}