package com.jongzazaal.jongmessage.base

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.jongzazaal.jongmessage.local.ShareManager
import com.sendbird.android.SendBird
import com.sendbird.calls.AuthenticateParams
import com.sendbird.calls.SendBirdCall
import com.sendbird.calls.SendBirdException
import com.sendbird.calls.User
import com.sendbird.calls.handler.AuthenticateHandler
import com.sendbird.calls.handler.CompletionHandler

//
//  BaseActivity.kt
//
//  JongMessage
//
//  Created by Jong on 1/4/2023 AD.
//
open class BaseActivity: AppCompatActivity() {

    open fun connectSendBird(){
        SendBird.connect(ShareManager.userId!!, object : SendBird.ConnectHandler{
            override fun onConnected(
                user: com.sendbird.android.User?,
                e: com.sendbird.android.SendBirdException?
            ) {
                if (e == null) {
                    // The user has been authenticated successfully and is connected to Sendbird server.
                    Log.d("tag2", "onConnected successfully")
                    setSendBird()
                    SendBird.updateCurrentUserInfo(ShareManager.userName, "", object : SendBird.UserInfoUpdateHandler{
                        override fun onUpdated(p0: com.sendbird.android.SendBirdException?) {

                        }

                    })
                }
                else{
                    Log.d("tag2", "onConnected failed")

                }
            }

        })
    }

    private fun setSendBird(){

        val params = AuthenticateParams(ShareManager.userId!!).setAccessToken(ShareManager.userToken)
        SendBirdCall.authenticate(params, object : AuthenticateHandler {
            override fun onResult(user: User?, e: SendBirdException?) {
                if (e == null) {
                    // The user has been authenticated successfully and is connected to Sendbird server.
                    Log.d("tag2", "authenticated successfully")
                    registerSendBird()
                }
                else{
                    Log.d("tag2", "authenticated failed")

                }
            }

        })
    }

    private fun registerSendBird(){
        SendBirdCall.registerPushToken(ShareManager.fcmToken?:"", false, object :
            CompletionHandler {
            override fun onResult(e: SendBirdException?) {
                if (e == null) {
                    // The user has been authenticated successfully and is connected to Sendbird server.
                    Log.d("tag2", "registerPushToken successfully")
//                    ReceiveListener()

                }
                else{
                    Log.d("tag2", "registerPushToken failed")

                }
            }

        })
    }
}