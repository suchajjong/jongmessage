package com.jongzazaal.jongmessage.fcm

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.media.RingtoneManager
import android.os.Build
import android.os.SystemClock
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.jongzazaal.jongmessage.R
import com.jongzazaal.jongmessage.local.ShareManager
import com.jongzazaal.jongmessage.module.MainActivity
import com.sendbird.calls.SendBirdCall
import kotlin.random.Random

//
//  MyFirebaseMessagingService.kt
//
//  JongMessage
//
//  Created by Jong on 1/4/2023 AD.
//
class MyFirebaseMessagingService: FirebaseMessagingService() {
    private val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
    private var mNotificationManager: NotificationManager? = null

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d("tag2", "onNewToken: $token")
        ShareManager.fcmToken = token
    }

    override fun onMessageReceived(remodeMessage: RemoteMessage) {
        super.onMessageReceived(remodeMessage)
        if (SendBirdCall.handleFirebaseMessageData(remodeMessage.data)){

        }
        else{
            val notificationManager = getNotificationManager()
            val notificationBuild = getNotification(remodeMessage)
            val oneTimeID = SystemClock.uptimeMillis().toInt()
            notificationManager.notify(oneTimeID, notificationBuild)
        }


    }

    private fun getNotification(remodeMessage: RemoteMessage): Notification {
        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel(getString(R.string.default_notification_channel_id), getString(
                    R.string.default_notification_channel_id))
            } else {
                // If earlier version channel ID is not used
                // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
                ""
            }
        val intent = MainActivity.getIntent(this)
        val randomValues = Random.nextInt(0, 9999)
        val pendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PendingIntent.getActivities(this, randomValues, arrayOf(intent), PendingIntent.FLAG_IMMUTABLE)
        } else {
            PendingIntent.getActivities(this, randomValues, arrayOf(intent), PendingIntent.FLAG_ONE_SHOT)

        }
        val notification = NotificationCompat.Builder(this, channelId)
            .setContentTitle(remodeMessage.notification?.title + "(private)")
            .setContentText(remodeMessage.notification?.body)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setLargeIcon(generateBitmapFromVectorDrawable(applicationContext, R.mipmap.ic_launcher))
            .setVibrate(longArrayOf(100, 200, 300, 400, 500, 400, 500, 200, 500))
            .setContentIntent(pendingIntent)
            .setSound(defaultSoundUri)
            .setDefaults(Notification.DEFAULT_ALL)
            .setStyle(NotificationCompat.BigTextStyle().bigText(remodeMessage.notification?.body))
            .setColor(ContextCompat.getColor(applicationContext, R.color.black))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setAutoCancel(true)


        return notification.build()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val chan = NotificationChannel(channelId,
            channelName, NotificationManager.IMPORTANCE_DEFAULT)
        chan.importance = NotificationManager.IMPORTANCE_DEFAULT
        chan.lightColor = Color.BLUE
        chan.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 500, 200, 500)

        getNotificationManager().createNotificationChannel(chan)
        return channelId
    }
    private fun getNotificationManager(): NotificationManager {
        if (mNotificationManager == null) {
            mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }
        return mNotificationManager as NotificationManager
    }
    fun generateBitmapFromVectorDrawable(context: Context, drawableId: Int): Bitmap {
        val drawable = ContextCompat.getDrawable(context, drawableId) as Drawable
        val bitmap = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )

        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)

        return bitmap
    }
}