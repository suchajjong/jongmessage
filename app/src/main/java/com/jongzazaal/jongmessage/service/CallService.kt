package com.jongzazaal.jongmessage.service

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.jongzazaal.jongmessage.R
import com.jongzazaal.jongmessage.model.CallServiceModel
import com.jongzazaal.jongmessage.module.CallLockScreenActivity
import com.jongzazaal.jongmessage.module.VoiceCallActivity
import com.sendbird.calls.DirectCall

//
//  CallService.kt
//
//  JongMessage
//
//  Created by Jong on 1/4/2023 AD.
//
class CallService: Service() {

    private var mContext: Context? = null
    private val mBinder: IBinder = CallBinder()
    private var mServiceData: CallServiceModel = CallServiceModel()

    inner class CallBinder : Binder() {
        val service: CallService
            get() = this@CallService
    }


    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        mContext = this
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        mServiceData = intent?.getParcelableExtra<CallServiceModel>(KEY_ITEM)?:CallServiceModel()
        updateNotification(mServiceData)
        return super.onStartCommand(intent, flags, startId)
    }
    fun updateNotification(serviceData: CallServiceModel) {
        mServiceData = serviceData
        startForeground(CallService.NOTIFICATION_ID, getNotification(mServiceData))
    }



//    fun stopService(context: Context?){
//        context?.let {
//            val intent = Intent(context, CallService::class.java)
//            context.stopService(intent)
//        }
//    }

    private fun getNotification(data: CallServiceModel): Notification {

        val currentTime = System.currentTimeMillis().toInt()
        val channelId = "sendbird_call"//mContext!!.packageName + currentTime
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val channelName = "sendbird calls"
            val channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH)

            val notificationManager = mContext!!.getSystemService(
                NotificationManager::class.java
            )
            notificationManager?.createNotificationChannel(channel)
        }

        val callAcceptIntent = VoiceCallActivity.getIntent(mContext!!, data.apply { this.isAccept = true })
        val callAcceptPendingIntent = PendingIntent.getActivity(
            mContext,
            currentTime + 1, callAcceptIntent, PendingIntent.FLAG_IMMUTABLE
        )

        val callDeclineIntent = VoiceCallActivity.getIntent(mContext!!, data.apply { this.isAccept = false })
        val callDeclinePendingIntent = PendingIntent.getActivity(
            mContext,
            currentTime + 2, callDeclineIntent, PendingIntent.FLAG_IMMUTABLE
        )

        val callLockScreenIntent = CallLockScreenActivity.getIntent(mContext!!, data.apply { this.isAccept = false })
        val callLockScreePendingIntent = PendingIntent.getActivity(
            mContext,
            currentTime + 3, callLockScreenIntent, PendingIntent.FLAG_IMMUTABLE
        )

        val builder = NotificationCompat.Builder(mContext!!, channelId)
        builder.setContentTitle(mServiceData.callerNickName)
            .setContentText("call")
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setCategory(NotificationCompat.CATEGORY_CALL)
            .setFullScreenIntent(callLockScreePendingIntent, true)

        if (mServiceData.isConnect == true){
            builder.addAction(NotificationCompat.Action(0, "Decline", callDeclinePendingIntent))
        }
        else{
            builder.addAction(NotificationCompat.Action(0, "Decline", callDeclinePendingIntent))
            builder.addAction(NotificationCompat.Action(0, "Accept", callAcceptPendingIntent))
        }
        return builder.build()
    }

    companion object {
        private val NOTIFICATION_ID = 1
        private val KEY_ITEM = "key_item"
        fun startService(context: Context, serviceData: CallServiceModel){
//            mServiceData = serviceData
            val intent = Intent(context, CallService::class.java)
            intent.putExtra(KEY_ITEM, serviceData)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent)
            }
            else{
                context.startService(intent)
            }
        }

        fun stopService(context: Context?){
            context?.let {
                val intent = Intent(context, CallService::class.java)
                context.stopService(intent)
            }
        }

        fun onRinging(context: Context?, call: DirectCall){
            context?.let {
                val data = CallServiceModel(
                    callId = call.callId,
                    callerId = call.caller?.userId,
                    callerNickName = call.caller?.nickname,
                    isConnect = false,
                    amICallee = true
                )
                startService(context, data)
            }
        }
        fun onConnect(context: Context?, call: DirectCall){
            context?.let {
                val data = CallServiceModel(
                    callId = call.callId,
                    callerId = call.remoteUser?.userId,
                    callerNickName = call.remoteUser?.nickname,
                    isConnect = true,
                    amICallee = call.remoteUser?.userId == call.caller?.userId
                )
                startService(context, data)
            }
        }
    }

}