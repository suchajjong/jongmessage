package com.jongzazaal.jongmessage.module

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import com.jongzazaal.jongmessage.base.BaseActivity
import com.jongzazaal.jongmessage.databinding.ActivityCallLockScreenBinding
import com.jongzazaal.jongmessage.local.ShareManager
import com.jongzazaal.jongmessage.model.CallServiceModel
import com.jongzazaal.jongmessage.utility.extension.showToast
import com.sendbird.android.SendBird
import com.sendbird.calls.*
import com.sendbird.calls.handler.AuthenticateHandler
import com.sendbird.calls.handler.DirectCallListener
import java.util.*

//
//  CallLockScreenActivity.kt
//
//  JongMessage
//
//  Created by Jong on 1/4/2023 AD.
//
class CallLockScreenActivity : BaseActivity() {
    private val binding: ActivityCallLockScreenBinding by lazy {
        ActivityCallLockScreenBinding.inflate(layoutInflater)
    }
    private val user_id = ShareManager.userId
    private val token = ShareManager.userToken
    private val nickname = ShareManager.userName

    private var mCallDurationTimer: Timer?  = null
    private var directCall: DirectCall? = null
    private var model: CallServiceModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        model = intent.getParcelableExtra(KEY_ITEM)

        showWhenLockedAndTurnScreenOn()

        binding.imageViewStart.setOnClickListener {
            VoiceCallActivity.start(this, model.apply { this!!.isAccept = true }!!)
            finish()
        }

        binding.imageViewEnd.setOnClickListener {
            VoiceCallActivity.start(this, model.apply { this!!.isAccept = false }!!)
            finish()

        }
        binding.textViewUserId.text = model?.callerNickName?:""
        directCall = SendBirdCall.getCall(model?.callId?:"")
        setListener(directCall)
        connectSendBird()
    }

    private fun showWhenLockedAndTurnScreenOn() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true)
            setTurnScreenOn(true)
        } else {
            window.addFlags(
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        or WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
            )
        }
    }

    override fun connectSendBird(){
        SendBird.connect(user_id, object : SendBird.ConnectHandler{
            override fun onConnected(
                user: com.sendbird.android.User?,
                e: com.sendbird.android.SendBirdException?
            ) {
                if (e == null) {
                    // The user has been authenticated successfully and is connected to Sendbird server.
                    Log.d("tag2", "onConnected successfully")
                    setSendBird()
                    SendBird.updateCurrentUserInfo(nickname, "", object : SendBird.UserInfoUpdateHandler{
                        override fun onUpdated(p0: com.sendbird.android.SendBirdException?) {

                        }

                    })
                }
                else{
                    Log.d("tag2", "onConnected failed")

                }
            }

        })
    }

    private fun setSendBird(){

        val params = AuthenticateParams(user_id!!).setAccessToken(token)
        SendBirdCall.authenticate(params, object : AuthenticateHandler {
            override fun onResult(user: User?, e: SendBirdException?) {
                if (e == null) {
                    // The user has been authenticated successfully and is connected to Sendbird server.
                    Log.d("tag2", "authenticated successfully")
                    Log.d("tag2", model.toString())
//                    if (model?.isAccept == true){
//                        if(model?.amICallee == true){
//                            val callOption = CallOptions().apply { setVideoEnabled(false); setAudioEnabled(true); }
//                            directCall?.accept(AcceptParams().setCallOptions(callOption))
//                        }
//                        else{
//                            startCall(model?.callerId!!)
//                        }
//                    }
//                    else{
//                        directCall?.end()
//                    }
                }
                else{
                    Log.d("tag2", "authenticated failed")

                }
            }

        })
    }

    private fun setListener(call: DirectCall?){
        call?.let {
            call.setListener(object : DirectCallListener(){
                override fun onConnected(call: DirectCall) {
//                    "Connected call".showToast()
//                    setCallDurationTimer(call)
//                    text_view_user_id.text = call.remoteUser?.nickname?:""\
//                    finish()
                }

                override fun onEnded(call: DirectCall) {
                    "end call".showToast()
//                    cancelCallDurationTimer()
//                    CallService.stopService(this@CallLockScreenActivity)
                    finish()
                }
            })
        }
    }

    companion object{
        private const val KEY_ITEM = "key_item"

        fun getIntent(context: Context, model: CallServiceModel): Intent {
            return (Intent(context, CallLockScreenActivity::class.java).apply {
                putExtra(KEY_ITEM, model)

            })
        }
    }
}