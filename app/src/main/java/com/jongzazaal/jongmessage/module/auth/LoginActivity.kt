package com.jongzazaal.jongmessage.module.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.jongzazaal.jongmessage.R
import com.jongzazaal.jongmessage.base.BaseActivity
import com.jongzazaal.jongmessage.base.BaseCommon
import com.jongzazaal.jongmessage.databinding.ActivityLoginBinding
import com.jongzazaal.jongmessage.local.ShareManager
import com.jongzazaal.jongmessage.utility.extension.showToast
import org.json.JSONException
import java.util.*

//
//  LoginActivity.kt
//
//  JongMessage
//
//  Created by Jong on 2/4/2023 AD.
//
class LoginActivity : BaseActivity(), BaseCommon {
    private val binding: ActivityLoginBinding by lazy { ActivityLoginBinding.inflate(layoutInflater) }
    private var mGoogleSignInClient: GoogleSignInClient? = null
    private var mAuth: FirebaseAuth? = null
    private var callbackManager: CallbackManager? = null

//    private val viewModel: AuthViewModel by lazy {
//        ViewModelProvider(this, AuthViewModel.Factory(application)).get(
//            AuthViewModel::class.java)
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initView()
        initListener()
        initViewModel()
        firstCreate()
//        logout()
    }
//    private fun logout(){
//        LogoutManager.logout()
//    }

    override fun firstCreate() {
        mAuth = FirebaseAuth.getInstance()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        callbackManager = CallbackManager.Factory.create();

    }

    override fun initView() {

    }

    override fun initListener() {
        binding.loginFacebook.setOnClickListener {
            signInFacebook()
        }
        binding.loginGoogle.setOnClickListener {
            signInGoogle()
        }
        binding.login.setOnClickListener {
//            LoginFirebaseActivity.start(this)
        }
        binding.register.setOnClickListener {
//            RegisterFirebaseActivity.start(this)
        }
    }

    override fun initViewModel() {

    }


    override fun showLoading() {

    }

    override fun hideLoading() {

    }
    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        val task = GoogleSignIn.getSignedInAccountFromIntent(result?.data)
        try {
            // Google Sign In was successful, authenticate with Firebase
            val account = task.getResult(ApiException::class.java)!!
            firebaseAuthWithGoogle(account.idToken!!)
        } catch (e: ApiException) {
            // Google Sign In failed, update UI appropriately
            "Google Sign In failed".showToast()
        }
    }

    private fun signInGoogle() {
        val signInIntent = mGoogleSignInClient?.signInIntent
        resultLauncher.launch(signInIntent)
    }
    private fun signInFacebook(){
        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult) {
                    // App code
                    val request = GraphRequest.newMeRequest(result.accessToken, GraphRequest.GraphJSONObjectCallback { _, response ->
                        hideLoading()
                        val json = response?.jsonObject
                        try {
                            if (json != null) {
                                val email = json.getString("email")
                                if (email.isNullOrEmpty()){
                                    "login_fb_no_email".showToast()
                                }
                                else{
                                    firebaseAuthWithFacebook(result.accessToken)
                                }

                            }
                        } catch (e: JSONException) {
                            "login_fb_no_email".showToast()
                        }
                    })

                    showLoading()
                    val parameters = Bundle()
                    parameters.putString("fields", "email")
                    request.parameters = parameters
                    request.executeAsync()

                }

                override fun onCancel() {
                    // App code
                    hideLoading()
                    "user cancel".showToast()
                }

                override fun onError(exception: FacebookException) {
                    // App code
                    hideLoading()
                    "login_fb_on_error".showToast()
                }
            })

        LoginManager.getInstance().logInWithReadPermissions(this, callbackManager!!, Arrays.asList("public_profile", "email"))
    }
    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    task.result!!.user!!.getIdToken(true).addOnSuccessListener {
                        ShareManager.userToken = it.token
                        ShareManager.userId = task.result!!.user?.email
                        ShareManager.userName = task.result!!.user?.displayName
                        "LoginSuccess".showToast()
                        finish()
                    }

                } else {
                    // If sign in fails, display a message to the user.
                    "firebaseAuthWithGoogle_sign in fails".showToast()
                }

                // ...
            }
    }
    private fun firebaseAuthWithFacebook(token: AccessToken) {
        val credential = FacebookAuthProvider.getCredential(token.token)
        mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    task.result!!.user!!.getIdToken(true).addOnSuccessListener {
                        ShareManager.userToken = it.token
                        ShareManager.userId = task.result!!.user?.email
                        ShareManager.userName = task.result!!.user?.displayName
                        "LoginSuccess".showToast()
                        finish()
                    }
                } else {
                    // If sign in fails, display a message to the user.
                    "firebaseAuthWithFacebook_sign in fails".showToast()
                }

                // ...
            }
    }


    companion object {

        fun start(context: Context) {
            context.startActivity(Intent(context, LoginActivity::class.java).apply {
            })
        }
        fun startClearTask(context: Context) {
            context.startActivity(Intent(context, LoginActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            })
        }
    }


}