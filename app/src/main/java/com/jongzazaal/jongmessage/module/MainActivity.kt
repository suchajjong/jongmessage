package com.jongzazaal.jongmessage.module

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.jongzazaal.jongmessage.base.BaseActivity
import com.jongzazaal.jongmessage.base.BaseCommon
import com.jongzazaal.jongmessage.databinding.ActivityMainBinding
import com.jongzazaal.jongmessage.local.ShareManager
import com.jongzazaal.jongmessage.module.auth.LoginActivity
import com.jongzazaal.jongmessage.module.auth.LogoutManager

class MainActivity : BaseActivity(), BaseCommon {
    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        if (!ShareManager.isLogin()){
            setLogin()
        }
        else{
            connectSendBird()
        }
        initView()
        initListener()
    }

    override fun firstCreate() {

    }

    override fun initView() {

    }

    override fun initListener() {
        binding.logout.setOnClickListener {
            LogoutManager.logout()
            updateUser()
        }

        binding.login.setOnClickListener {
            setLogin()
        }

        binding.call.setOnClickListener {
            CallMainActivity.start(this)
        }
    }

    override fun initViewModel() {

    }

    override fun showLoading() {

    }

    override fun hideLoading() {

    }

    override fun onResume() {
        super.onResume()
        updateUser()
    }

    private fun updateUser(){
        binding.user.text = ShareManager.userName + ShareManager.userId
    }


    private fun setLogin(){
        LoginActivity.start(this)
    }

    companion object {
        private const val PAGE_ID = "page_id"

        fun start(context: Context) {
            context.startActivity(Intent(context, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

            })
        }
        fun getIntent(context: Context): Intent {
            return (Intent(context, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

            })
        }
    }


}