package com.jongzazaal.jongmessage.module.auth

import android.app.NotificationManager
import android.content.Context
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import com.jongzazaal.jongmessage.R
import com.jongzazaal.jongmessage.local.ContexterManager
import com.jongzazaal.jongmessage.local.ShareManager
import java.io.IOException

//
//  LogoutManager.kt
//
//  JongMessage
//
//  Created by Jong on 2/4/2023 AD.
//
object LogoutManager {
    fun logout(){
        val context = ContexterManager.getInstance().getApplicationContext()
        val mAuth = FirebaseAuth.getInstance()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(context.getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        val mGoogleSignInClient = GoogleSignIn.getClient(ContexterManager.getInstance().getApplicationContext(), gso)

        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
        LoginManager.getInstance().logOut()
        mGoogleSignInClient.signOut()
        mAuth.signOut()
        ShareManager.logout()

//        try {
//            FirebaseMessaging.getInstance().deleteToken()
//        }catch (e: IOException){
//            e.printStackTrace()
//        }
    }
}