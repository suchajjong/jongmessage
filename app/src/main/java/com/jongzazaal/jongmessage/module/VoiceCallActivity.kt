package com.jongzazaal.jongmessage.module

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.jongzazaal.jongmessage.base.BaseActivity
import com.jongzazaal.jongmessage.databinding.ActivityVoiceCallBinding
import com.jongzazaal.jongmessage.local.ShareManager
import com.jongzazaal.jongmessage.model.CallServiceModel
import com.jongzazaal.jongmessage.service.CallService
import com.jongzazaal.jongmessage.utility.TimeUtils
import com.jongzazaal.jongmessage.utility.extension.showToast
import com.sendbird.android.SendBird
import com.sendbird.calls.*
import com.sendbird.calls.handler.AuthenticateHandler
import com.sendbird.calls.handler.DirectCallListener
import java.util.*

//
//  VoiceCallActivity.kt
//
//  JongMessage
//
//  Created by Jong on 1/4/2023 AD.
//
class VoiceCallActivity: BaseActivity() {
//    private val binding: ActivityVoiceCallBinding lazy { ActivityVoiceCallBinding.inflate(layoutInflater) }

    private val binding: ActivityVoiceCallBinding by lazy {
        ActivityVoiceCallBinding.inflate(layoutInflater)
    }
    private val user_id = ShareManager.userId
    private val token = ShareManager.userToken
    private val nickname = ShareManager.userName

    private var mCallDurationTimer: Timer?  = null
    private var directCall: DirectCall? = null
    private var model: CallServiceModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        model = intent.getParcelableExtra(KEY_ITEM)
        binding.imageViewEnd.setOnClickListener {
            directCall?.end()
        }
        directCall = SendBirdCall.getCall(model?.callId?:"")
        setListener(directCall)
        connectSendBird()

    }
    override fun connectSendBird(){
        SendBird.connect(user_id, object : SendBird.ConnectHandler{
            override fun onConnected(
                user: com.sendbird.android.User?,
                e: com.sendbird.android.SendBirdException?
            ) {
                if (e == null) {
                    // The user has been authenticated successfully and is connected to Sendbird server.
                    Log.d("tag2", "onConnected successfully")
                    setSendBird()
                    SendBird.updateCurrentUserInfo(nickname, "", object : SendBird.UserInfoUpdateHandler{
                        override fun onUpdated(p0: com.sendbird.android.SendBirdException?) {

                        }

                    })
                }
                else{
                    Log.d("tag2", "onConnected failed")

                }
            }

        })
    }

    private fun setSendBird(){

        val params = AuthenticateParams(user_id!!).setAccessToken(token)
        SendBirdCall.authenticate(params, object : AuthenticateHandler {
            override fun onResult(user: User?, e: SendBirdException?) {
                if (e == null) {
                    // The user has been authenticated successfully and is connected to Sendbird server.
                    Log.d("tag2", "authenticated successfully")
                    Log.d("tag2", model.toString())
                    if (model?.isAccept == true){
                        if(model?.amICallee == true){
                            val callOption = CallOptions().apply { setVideoEnabled(false); setAudioEnabled(true); }
                            directCall?.accept(AcceptParams().setCallOptions(callOption))
                        }
                        else{
                            startCall(model?.callerId!!)
                        }
                    }
                    else{
                        directCall?.end()
                    }
                }
                else{
                    Log.d("tag2", "authenticated failed")

                }
            }

        })
    }

    private fun setCallDurationTimer(call: DirectCall) {
        if (mCallDurationTimer == null) {
            mCallDurationTimer = Timer()
            mCallDurationTimer!!.schedule(object : TimerTask() {
                override fun run() {
                    runOnUiThread {
                        val callDuration: String = TimeUtils.getTimeString(call.duration)
                        binding.textViewStatus.text = callDuration
                    }
                }
            }, 0, 1000)
        }
    }
    private fun cancelCallDurationTimer() {
        if (mCallDurationTimer != null) {
            mCallDurationTimer!!.cancel()
            mCallDurationTimer = null
        }
    }

    private fun startCall(callerId: String){
        val callOption = CallOptions().apply { setVideoEnabled(false); setAudioEnabled(true) }
        val params = DialParams(callerId)
        params.setVideoCall(false)
        params.setCallOptions(callOption)

//        directCall = SendBirdCall.dial(params){ call, error ->
//            if (error == null){
//                Log.d("tag2", "dial successfully")
//            }
//            else{
//                Log.d("tag2", "dial failed")
//            }
//        }
//        setListener(directCall)

    }
    private fun acceptCall(){

    }

    private fun setListener(call: DirectCall?){
        call?.let {
            call.setListener(object : DirectCallListener(){
                override fun onConnected(call: DirectCall) {
                    "Connected call".showToast()
                    setCallDurationTimer(call)
                    binding.textViewUserId.text = call.remoteUser?.nickname?:""
                }

                override fun onEnded(call: DirectCall) {
                    "end call".showToast()
                    cancelCallDurationTimer()
                    CallService.stopService(this@VoiceCallActivity)
                    finish()
                }
            })
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        cancelCallDurationTimer()
    }

    companion object {
        private const val KEY_ITEM = "key_item"
        fun start(context: Context, model: CallServiceModel) {
            context.startActivity(Intent(context, VoiceCallActivity::class.java).apply {
                putExtra(KEY_ITEM, model)
            })
        }

        fun getIntent(context: Context, model: CallServiceModel): Intent {
            return (Intent(context, VoiceCallActivity::class.java).apply {
                putExtra(KEY_ITEM, model)

            })
        }
    }

}