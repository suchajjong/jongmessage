package com.jongzazaal.jongmessage.module

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.firebase.messaging.FirebaseMessaging
import com.jongzazaal.jongmessage.base.BaseActivity
import com.jongzazaal.jongmessage.databinding.ActivityCallMainBinding
import com.jongzazaal.jongmessage.databinding.ActivityMainBinding
import com.jongzazaal.jongmessage.local.ShareManager
import com.jongzazaal.jongmessage.utility.extension.gone
import com.jongzazaal.jongmessage.utility.extension.showToast
import com.jongzazaal.jongmessage.utility.extension.visible
import com.sendbird.android.SendBird
import com.sendbird.calls.*
import com.sendbird.calls.handler.*

//
//  CallMainActivity.kt
//
//  JongMessage
//
//  Created by Jong on 1/4/2023 AD.
//
class CallMainActivity : BaseActivity() {
    private val user_id = ShareManager.userId
    private val token = ShareManager.userToken
    private val nickname = ShareManager.userName

    private val binding: ActivityCallMainBinding by lazy {
        ActivityCallMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        requestPermission()

        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.d("tag2", "Fetching FCM registration token failed", task.exception)
                return@addOnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result
            Log.d("tag2", "Token: $token")
            ShareManager.fcmToken = token
            connectSendBird()
        }

        binding.callJongzazaal.setOnClickListener {
            makeCall("jongzazaal@gmail.com")
        }
        binding.callArm19jong.setOnClickListener {
            makeCall("arm19jong@gmail.com")
        }
        binding.callEnd.setOnClickListener {
            directCall?.end()
        }
        binding.callAccept.setOnClickListener {
            directCall?.accept(AcceptParams())
            binding.callAccept.gone()
            "accept call".showToast()
        }
    }

    override fun connectSendBird(){
        SendBird.connect(user_id, object : SendBird.ConnectHandler{
            override fun onConnected(
                user: com.sendbird.android.User?,
                e: com.sendbird.android.SendBirdException?
            ) {
                if (e == null) {
                    // The user has been authenticated successfully and is connected to Sendbird server.
                    Log.d("tag2", "onConnected successfully")
                    setSendBird()
                    SendBird.updateCurrentUserInfo(nickname, "", object : SendBird.UserInfoUpdateHandler{
                        override fun onUpdated(p0: com.sendbird.android.SendBirdException?) {

                        }

                    })
                }
                else{
                    Log.d("tag2", "onConnected failed")

                }
            }

        })
    }

    private fun setSendBird(){

        val params = AuthenticateParams(user_id!!).setAccessToken(token)
        SendBirdCall.authenticate(params, object : AuthenticateHandler {
            override fun onResult(user: User?, e: SendBirdException?) {
                if (e == null) {
                    // The user has been authenticated successfully and is connected to Sendbird server.
                    Log.d("tag2", "authenticated successfully")
                    registerSendBird()
                }
                else{
                    Log.d("tag2", "authenticated failed")

                }
            }

        })
    }

    private fun registerSendBird(){
        SendBirdCall.registerPushToken(ShareManager.fcmToken?:"", false, object :
            CompletionHandler {
            override fun onResult(e: SendBirdException?) {
                if (e == null) {
                    // The user has been authenticated successfully and is connected to Sendbird server.
                    Log.d("tag2", "registerPushToken successfully")
                    ReceiveListener()

                }
                else{
                    Log.d("tag2", "registerPushToken failed")

                }
            }

        })
    }

    private fun  makeCall(callerId: String){
        val params = DialParams(callerId)
        params.setVideoCall(false)
        params.setCallOptions(CallOptions())

        Log.d("tag2", "1")
        val call = SendBirdCall.dial(params, object : DialHandler {
            override fun onResult(call: DirectCall?, e: SendBirdException?) {
                if (e == null) {
                    // The user has been authenticated successfully and is connected to Sendbird server.
                    call?.setListener(object : DirectCallListener() {
                        override fun onEstablished(call: DirectCall) {}
                        override fun onConnected(call: DirectCall) {
                            "connect".showToast()
                        }
                        override fun onEnded(call: DirectCall) {
//                            call.end()
                            "end call".showToast()
                            binding.callAccept.gone()
                        }
                        override fun onRemoteAudioSettingsChanged(call: DirectCall) {}
                    })
                    Log.d("tag2", "dial successfully")
                    this@CallMainActivity.directCall = call
                }
                else{
                    Log.d("tag2", "dial failed")

                }
            }

        })
    }

    private var directCall: DirectCall? = null
    private fun ReceiveListener(){
        SendBirdCall.addListener("1234", object : SendBirdCallListener() {
            override fun onRinging(call: DirectCall) {
                call.setListener(object : DirectCallListener() {
                    override fun onEstablished(call: DirectCall) {}
                    override fun onConnected(call: DirectCall) {
                        "connect".showToast()
                    }
                    override fun onEnded(call: DirectCall) {
//                        call.end()
                        "end call".showToast()
                        binding.callAccept.gone()
                    }
                    override fun onRemoteAudioSettingsChanged(call: DirectCall) {}
                })
//                call.accept(AcceptParams())
                this@CallMainActivity.directCall = call
                binding.callAccept.visible()
                binding.callAccept.text = "accept(${call.caller?.nickname})"
            }

        })
    }

    fun requestPermission(){
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.BLUETOOTH_CONNECT), 123)
    }

    companion object {
        private const val PAGE_ID = "page_id"

        fun start(context: Context) {
            context.startActivity(Intent(context, CallMainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

            })
        }
//        fun getIntent(context: Context): Intent {
//            return (Intent(context, MainActivity::class.java).apply {
//                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//
//            })
//        }
    }
}