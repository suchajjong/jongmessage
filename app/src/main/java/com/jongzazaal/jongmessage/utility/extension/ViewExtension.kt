package com.jongzazaal.jongmessage.utility.extension

import android.view.View
import android.widget.Toast
import com.jongzazaal.jongmessage.local.ContexterManager

fun View.gone() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun Any.showToast() {
    Toast.makeText(ContexterManager.getInstance().getApplicationContext(), "${this}", Toast.LENGTH_SHORT).show()
}