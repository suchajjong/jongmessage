package com.jongzazaal.jongmessage.local

import android.content.Context

//
//  SharePrefManagerNotSecure.kt
//
//  JongMessage
//
//  Created by Jong on 1/4/2023 AD.
//
object SharePrefManagerNotSecure {
    val context = ContexterManager.getInstance().getApplicationContext()
    val sharedPreferencesSecureNotSecure = context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE);

    fun getString(key:String) = sharedPreferencesSecureNotSecure.getString(key, "")
    fun getBoolean(key:String, defaultValue: Boolean = false) = sharedPreferencesSecureNotSecure.getBoolean(key, defaultValue)
    fun getInt(key:String) = sharedPreferencesSecureNotSecure.getInt(key, 0)
    fun getLong(key:String) = sharedPreferencesSecureNotSecure.getLong(key, 0L)
    fun getFloat(key:String) = sharedPreferencesSecureNotSecure.getFloat(key, 0f)

    fun putString(key:String, value:String?){
        sharedPreferencesSecureNotSecure.edit().putString(key, value).apply()}
    fun putBoolean(key:String, value:Boolean){
        sharedPreferencesSecureNotSecure.edit().putBoolean(key, value).apply()}
    fun putInt(key:String, value:Int){
        sharedPreferencesSecureNotSecure.edit().putInt(key, value).apply()}
    fun putLong(key:String, value:Long){
        sharedPreferencesSecureNotSecure.edit().putLong(key, value).apply()}
    fun putFloat(key:String, value:Float){
        sharedPreferencesSecureNotSecure.edit().putFloat(key, value).apply()}

    fun clearSharePreferences(){
        sharedPreferencesSecureNotSecure.edit().clear().apply()
    }
}