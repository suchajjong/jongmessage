package com.jongzazaal.jongmessage.local

//
//  ShareManager.kt
//
//  JongMessage
//
//  Created by Jong on 1/4/2023 AD.
//
object ShareManager {
    private val KEY_TAG_FCM = "key_tag_fcm"
    private val KEY_USER_ID = "key_user_id"
    private val KEY_USER_TOKEN = "key_user_token"
    private val KEY_USER_NAME = "key_user_name"

    /**
     * setting
     */
    var fcmToken: String?
        get() = SharePrefManagerNotSecure.getString(KEY_TAG_FCM)
        set(value) = SharePrefManagerNotSecure.putString(KEY_TAG_FCM, value)

    var userId: String?
        get() = SharePrefManagerNotSecure.getString(KEY_USER_ID)
        set(value) = SharePrefManagerNotSecure.putString(KEY_USER_ID, value)

    var userToken: String?
        get() = SharePrefManagerNotSecure.getString(KEY_USER_TOKEN)
        set(value) = SharePrefManagerNotSecure.putString(KEY_USER_TOKEN, value)

    var userName: String?
        get() = SharePrefManagerNotSecure.getString(KEY_USER_NAME)
        set(value) = SharePrefManagerNotSecure.putString(KEY_USER_NAME, value)

    fun logout(){
        SharePrefManagerNotSecure.clearSharePreferences()
    }
    fun isLogin():Boolean{
        val token = fcmToken
        return (!userToken.isNullOrEmpty())
        fcmToken = token
    }
}